Chicago Diamond Buyer is a company of more than 15 years in the diamond business. We strive to provide attractive prices for your diamonds. The company has earned its name by delivering quality service and giving people new opportunities for a fresh start. Chicago Diamond Buyer was the initiative of three brothers with an impactful background in the diamond industry. Chicago Diamond Buyers is located in the heart of the diamond district of Chicago.

Our company offers the best prices on AGS, EGL and GIA certification diamonds but also deals in uncertified diamond stones. We have expert Gemologists who evaluate the quality and design of the diamond and use our time-tested system for estimating the best price possible.

Website: https://chicagodiamondbuyer.net/
